package com.ohpen.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OhpenAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(OhpenAssignmentApplication.class, args);
	}

}
