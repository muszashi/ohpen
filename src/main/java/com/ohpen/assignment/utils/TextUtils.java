package com.ohpen.assignment.utils;


import org.apache.commons.lang.WordUtils;
import org.springframework.stereotype.Component;

@Component
public class TextUtils {

    public String replaceAllNonAlphaNumeric(String text) {
        return text.replaceAll("[^A-Za-z0-9]", " ");
    }

    public String capitalizeFully(String text) {
        return WordUtils.capitalizeFully(replaceAllNonAlphaNumeric(text)).replaceAll(" ", "");
    }
}
