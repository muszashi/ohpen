package com.ohpen.assignment.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@Component
public class DataReader {

    @Autowired
    private TextUtils textUtils;

    public String getData(final Object target, final String fieldName) {
        try {
            String capitalizedText = textUtils.capitalizeFully(fieldName);
            String getterMethodName = "get" + capitalizedText;
            String isMethodName = "is" + capitalizedText;

            Method getterMethod = Arrays.stream(target.getClass().getDeclaredMethods())
                                        .filter(method -> (method.getName().equals(getterMethodName) || method.getName().equals(isMethodName)))
                                        .findFirst()
                                        .orElseThrow(() -> new RuntimeException("Method not found: " + fieldName));


            return String.valueOf(getterMethod.invoke(target));
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("Method call failed", e);
        }
    }
}
