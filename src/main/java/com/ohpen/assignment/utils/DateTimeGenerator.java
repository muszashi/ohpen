package com.ohpen.assignment.utils;

import com.ohpen.exception.OhpenTestException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.ohpen.assignment.utils.GenericConstants.GENERIC_DATE_TIME_FORMATTER;
import static com.ohpen.assignment.utils.GenericConstants.INPUT_DATE_FORMATTER;
import static com.ohpen.assignment.utils.GenericConstants.PLACEHOLDER_DATE;
import static com.ohpen.assignment.utils.GenericConstants.PLACEHOLDER_DATE_TIME;
import static com.ohpen.assignment.utils.GenericConstants.TIMESTAMP_FORMATTER;

@Component
public class DateTimeGenerator {

    private static final String SECOND = "s";
    private static final String MINUTE = "m";
    private static final String HOUR = "h";
    private static final String DAY = "d";
    private static final String WEEK = "w";
    private static final String MONTH = "M";
    private static final String YEAR = "Y";
    private static final String SUBTRACT_OPERATOR = "-";
    private static final String ADD_OPERATOR = "+";


    public String generateDateString(String value) {
        return generateDateTime(value).format(INPUT_DATE_FORMATTER);
    }

    public String generateTimeStampString(String value) {
        return generateDateTime(value).format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public LocalDate generateDate(String value) {
        return generateDateTime(value).toLocalDate();
    }

    public LocalDateTime generateDateTime(String value) {
        LocalDateTime dateTime = LocalDateTime.now();
        if (StringUtils.isEmpty(value) || value.equals(PLACEHOLDER_DATE) || value.equals(PLACEHOLDER_DATE_TIME)) {
            return dateTime;
        }
        if (value.contains(PLACEHOLDER_DATE) || value.contains(PLACEHOLDER_DATE_TIME)) {
            String usableVal = value.replace(PLACEHOLDER_DATE, "").replace(PLACEHOLDER_DATE_TIME, "");
            String[] dateMutations = usableVal.split("&");
            for (String dateMutation : dateMutations) {
                dateTime = calculateDateTime(dateTime, dateMutation);
            }
        } else {
            dateTime = LocalDateTime.parse(value, GENERIC_DATE_TIME_FORMATTER);
        }
        return dateTime;
    }

    private LocalDateTime calculateDateTime(LocalDateTime dateTime, String value) {
        LocalDateTime calcDateTime;
        int length = value.length();
        String operator = value.substring(0, 1);
        String timeOperator = value.substring(length - 1, length);
        int calcValue = Integer.parseInt(value.substring(1, length - 1));
        switch (timeOperator) {
            case SECOND:
                calcDateTime = isAddOperator(operator) ? dateTime.plusSeconds(calcValue) : dateTime.minusSeconds(calcValue);
                break;
            case MINUTE:
                calcDateTime = isAddOperator(operator) ? dateTime.plusMinutes(calcValue) : dateTime.minusMinutes(calcValue);
                break;
            case HOUR:
                calcDateTime = isAddOperator(operator) ? dateTime.plusHours(calcValue) : dateTime.minusHours(calcValue);
                break;
            case DAY:
                calcDateTime = isAddOperator(operator) ? dateTime.plusDays(calcValue) : dateTime.minusDays(calcValue);
                break;
            case WEEK:
                calcDateTime = isAddOperator(operator) ? dateTime.plusWeeks(calcValue) : dateTime.minusWeeks(calcValue);
                break;
            case MONTH:
                calcDateTime = isAddOperator(operator) ? dateTime.plusMonths(calcValue) : dateTime.minusMonths(calcValue);
                break;
            case YEAR:
                calcDateTime = isAddOperator(operator) ? dateTime.plusYears(calcValue) : dateTime.minusYears(calcValue);
                break;
            default:
                throw new OhpenTestException("Not supported date/time operator: " + timeOperator);
        }
        return calcDateTime;
    }

    private boolean isAddOperator(String operator) {
        switch (operator) {
            case ADD_OPERATOR:
                return true;
            case SUBTRACT_OPERATOR:
                return false;
            default:
                throw new OhpenTestException("Not supported operator: " + operator);
        }
    }
}
