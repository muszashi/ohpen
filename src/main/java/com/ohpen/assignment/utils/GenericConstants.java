package com.ohpen.assignment.utils;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MILLI_OF_SECOND;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;

public final class GenericConstants {

    public static final String APPLICATION_JSON_CONTENT_TYPE = "application/json";
    public static final String NO_PLACEHOLDER = "no placeholder";
    public static final String PLACEHOLDER_HEADER_TITLE_COUNT = "[header title count]";
    public static final String PLACEHOLDER_CONTAINS = "[contains]";
    public static final String PLACEHOLDER_STARTS_WITH = "[starts with]";
    public static final String PLACEHOLDER_DATE = "[date]";
    public static final String PLACEHOLDER_DATE_TIME = "[datetime]";

    public static final DateTimeFormatter INPUT_DATE_FORMATTER = ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter TIMESTAMP_FORMATTER = ofPattern("YYYY-MM-DDTHH:MM:SSZ");

    public static final DateTimeFormatter GENERIC_DATE_TIME_FORMATTER = new DateTimeFormatterBuilder().appendOptional(ofPattern("dd-MM-yyyy HH:mm:ss"))
                                                                                                      .appendOptional(ofPattern("dd-MM-YY"))
                                                                                                      .appendOptional(ofPattern("YYYY-MM-DD HH:mm:ss"))
                                                                                                      .appendOptional(ofPattern("yyyy-MM-dd"))
                                                                                                      .appendOptional(ofPattern("yyyy-MM-dd'T'HH:mm:ss:SSSZZ"))
                                                                                                      .appendOptional(ofPattern("yyyy-MM-ddZZ"))
                                                                                                      .appendOptional(ofPattern("dd MMM yyy"))
                                                                                                      .parseDefaulting(HOUR_OF_DAY, 0)
                                                                                                      .parseDefaulting(MINUTE_OF_HOUR, 0)
                                                                                                      .parseDefaulting(SECOND_OF_MINUTE, 0)
                                                                                                      .parseDefaulting(MILLI_OF_SECOND, 0)
                                                                                                      .toFormatter();
}
