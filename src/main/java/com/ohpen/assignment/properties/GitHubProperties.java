package com.ohpen.assignment.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("github")
@Data
public class GitHubProperties {

    private String baseUri;
    private int basePort;
    private String basePath;
    private String accessToken;
}
