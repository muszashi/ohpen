package com.ohpen.assignment.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("notification")
@Data
public class NotificationProperties {
    private String user;
    private String repository;
}
