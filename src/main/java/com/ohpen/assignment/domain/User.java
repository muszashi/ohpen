package com.ohpen.assignment.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class User {

    private String login;
    private long id;
    private String nodeId;
    private String avatarUrl;
    private String gravatarId;
    private String url;
    private String htmlUrl;
    private String followersUrl;
    private String followingUrl;
    private String gistsUrl;
    private String starredUrl;
    private String subscriptionsUrl;
    private String organizationsUrl;
    private String reposUrl;
    private String eventsUrl;
    private String receivedEventsUrl;
    private String type;
    private boolean siteAdmin;
    private String name;
    private String company;
    private String blog;
    private String location;
    private String email;
    private String hireable;
    private String bio;
    private int publicRepos;
    private int publicGists;
    private int followers;
    private int following;
    private String createdAt;
    private String updatedAt;
    private int privateGists;
    private int totalPrivateRepos;
    private int ownedPrivateRepos;
    private int diskUsage;
    private int collaborators;
    private boolean twoFactorAuthentication;
    private Plan plan;

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    class Plan {
        private String name;
        private long space;
        private int collaborators;
        private int privateRepos;
    }
}
