package com.ohpen.assignment.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Param {

    private String paramName;
    private String paramValue;
}
