package com.ohpen.assignment.api;

import com.ohpen.assignment.properties.GitHubProperties;
import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.ohpen.assignment.utils.GenericConstants.APPLICATION_JSON_CONTENT_TYPE;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@Component
public class GitHubApi {

    private final Logger log = LoggerFactory.getLogger(GitHubApi.class);

    @Autowired
    private GitHubProperties gitHubProperties;

    private RequestSpecification specification;

    public ValidatableResponse get(final String path) {
        return get(path, null);
    }

    public ValidatableResponse get(final String path, final List<Param> params) {
        log.info("Executing get " + path);
        RequestSpecification specification = defaultSpecification().when()
                                                                   .contentType(APPLICATION_JSON_CONTENT_TYPE);

        if (isNotEmpty(params)) {
            params.forEach(param -> specification.pathParam(param.getParamName(), param.getParamValue()));
        }

        return specification
                .get(path)
                .then();
    }

    public ValidatableResponse put(final String path, final Object body, final List<Param> params) {
        log.info("Executing get " + path);
        RequestSpecification specification = defaultSpecification().when()
                                                                   .contentType(APPLICATION_JSON_CONTENT_TYPE)
                                                                   .body(body);
        if (isNotEmpty(params)) {
            params.forEach(param -> specification.pathParam(param.getParamName(), param.getParamValue()));
        }
        return specification.put(path)
                            .then();
    }

    @PostConstruct
    private void init() {
        specification = RestAssured.given()
                                   .log()
                                   .all(true)
                                   .spec(new RequestSpecBuilder().setUrlEncodingEnabled(false)
                                                                 .setPort(gitHubProperties.getBasePort())
                                                                 .setBaseUri(gitHubProperties.getBaseUri())
                                                                 .setBasePath(gitHubProperties.getBasePath())
                                                                 .build());
    }

    private RequestSpecification defaultSpecification() {
        return RestAssured.given()
                          .spec(specification)
                          .auth()
                          .preemptive()
                          .oauth2(gitHubProperties.getAccessToken());
    }


}
