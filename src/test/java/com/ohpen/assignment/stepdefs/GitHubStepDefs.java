package com.ohpen.assignment.stepdefs;

import com.ohpen.assignment.api.GitHubApi;
import com.ohpen.assignment.api.Param;
import com.ohpen.assignment.domain.NotificationTimeStamp;
import com.ohpen.assignment.domain.User;
import com.ohpen.assignment.properties.NotificationProperties;
import com.ohpen.assignment.utils.DataReader;
import com.ohpen.assignment.utils.DateTimeGenerator;
import io.cucumber.datatable.DataTable;
import io.cucumber.java8.En;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GitHubStepDefs implements En {

    private final Logger log = LoggerFactory.getLogger(GitHubStepDefs.class);

    @Autowired
    private GitHubApi api;

    @Autowired
    private DataReader dataReader;

    @Autowired
    private DateTimeGenerator dateTimeGenerator;

    @Autowired
    private NotificationProperties notificationProperties;

    Object responseObject;
    NotificationTimeStamp notificationTimeStamp;
    ValidatableResponse validatableResponse;

    public GitHubStepDefs() {


        When("^user gets information user API$", () -> responseObject = api.get("user")
                                                                           .statusCode(HttpStatus.SC_OK)
                                                                           .log()
                                                                           .all()
                                                                           .extract()
                                                                           .body()
                                                                           .as(User.class));

        When("^user gets authorizations for the predefined user", () -> {
            validatableResponse = api.get("authorizations")
                                .log()
                                .all();
        });

        When("^notifications set to read at (.+)$", (final String readTimeStamp) -> {
            notificationTimeStamp = new NotificationTimeStamp(dateTimeGenerator.generateTimeStampString(readTimeStamp));
            List<Param> params = new ArrayList<>();
            params.add(new Param("user", notificationProperties.getUser()));
            params.add(new Param("repository", notificationProperties.getRepository()));
            validatableResponse = api.put("repos/{user}/{repository}/notifications", notificationTimeStamp, params);

        });

        Then("^the (.+) data contains:$", (final String dataObjectName, final DataTable expectedData) -> {
            log.info("Verifying {} data", dataObjectName);
            expectedData.asMap(String.class, String.class)
                        .forEach((k, v) -> assertEquals(v, dataReader.getData(responseObject, (String) k)));
        });

        Then("^the notification set to read$", () -> {
            validatableResponse.statusCode(HttpStatus.SC_RESET_CONTENT);
        });

        Then ("^expected 403 error$", () -> validatableResponse.statusCode(HttpStatus.SC_FORBIDDEN));
    }
}
