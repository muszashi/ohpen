package com.ohpen.assignment.stepdefs;

import com.ohpen.assignment.OhpenAssignmentApplication;
import io.cucumber.core.api.Scenario;
import io.cucumber.java8.En;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = OhpenAssignmentApplication.class, loader = SpringBootContextLoader.class)
public class Hooks implements En {

    private static final String SYSTEM_PROPERTY_LINE_SEPARATOR = "line.separator";

    private static Logger logger = LoggerFactory.getLogger(Hooks.class);


    public Hooks() {

        Before(() -> {
            logger.info("Test started");
        });

        After((Scenario scenario) -> {
            if (scenario.isFailed()) {
                scenario.write("Current URL: " + System.getProperty(SYSTEM_PROPERTY_LINE_SEPARATOR + ""));
            }
            logger.info("Test finished");
        });
    }
}
