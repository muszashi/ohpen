package com.ohpen.assignment;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true,
        plugin = {"pretty",
                "html:target/cucumber",
                "json:target/cucumber/cucumber-json-report.json"},
        extraGlue = {"com.ohpen.assignment"},
        features = {"src/test/resources/features"},
        tags = {"not @Ignore"}
        )
public class RunCucumberTest {
}
