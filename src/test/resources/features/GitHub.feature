Feature: GitHub basic API test for Ohpen test assignment .GitHub basic

  @HappyFlow
  Scenario: Get current user data - Logged in user: Muszashi
    When user gets information user API
    Then the user data contains:
      | login     | Muszashi                                    |
      | name      | Ferenc Vizsy                                |
      | html_url  | https://github.com/Muszashi                 |
      | type      | User                                        |
      | repos_url | https://api.github.com/users/Muszashi/repos |

  @HappyFlow
  Scenario: Mark notifications as read
    When notifications set to read at [datetime]
    Then the notification set to read

  @ErrorFlow
  Scenario: Get repository url
    When user gets authorizations for the predefined user
    Then expected 403 error
