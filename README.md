# Test Automation Assignment for Ophen

# Project description
Create automated tests for Github.com API.

# Project/technology description
## Base project setup:
* Java 8
* Spring boot
* Maven
* Cucumber
* RestAssured

## Plugins:
* spring-boot-maven-plugin - Provides Spring boot support for maven
* maven-cucumber-reporting - Create cucumber HTML report
* maven-surefire-plugin - Execute the integration test

## Version control:
The project is using 
* Git for version control
* BitBucket as repository -> https://bitbucket.org/muszashi/bitly-interface-test

# Project/technology description
## Base project setup:
* Java 8
* Spring boot
* Maven
* Cucumber
* RestAssured

## Plugins:
* spring-boot-maven-plugin - Provides Spring boot support for maven
* maven-cucumber-reporting - Create cucumber HTML report
* maven-failsafe-plugin - Execute the integration test

## Version control:
The project is using 
* Git for version control
* GitHub as repository -> https://github.com/Muszashi/Ohpen

# Test execution:
The test are executed with the following maven command:
`mvn clean verify`

# Test report
Test report are generated and placed into the target folder:
`target/cucumber/cucumber-html-reports`

# Used sources
* Quick setup of SpringBoot project - https://start.spring.io/

# Pipeline 
Used Circle CI as pipeline

# Accomplished tasks:
* 3 endpoints with at least 2 different verbs - 2 gets 1 put
* 1 external data source with input and expected values - Use feature file and yaml file
* Json schema validation - By parsing the answer and converted to domain object
* Basic test report
* Human readable test report which can easily be used for troubleshooting without access to logs -> Cucumber report
* Extend with environment configuration -> Environment settings are coming from the application.yaml file

#Problems:
I ran into several issues with circle CI, where I had to spend quite a time to troubleshoot. Finally I found workaround but this is not ideal.
https://discuss.circleci.com/t/the-forked-vm-terminated-without-properly-saying-goodbye-while-running-tests/26246/8
https://stackoverflow.com/questions/53326285/failed-to-execute-maven-surefire-plugin-but-only-with-circleci

Also - I have created personal token with very limited access (according to keep security high) but GitHub revoked the token


